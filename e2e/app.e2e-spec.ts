import { eboxTemplatePage } from './app.po';

describe('ebox App', function() {
  let page: eboxTemplatePage;

  beforeEach(() => {
    page = new eboxTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
