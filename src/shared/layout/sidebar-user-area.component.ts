import { DepartmentServiceProxy, EmployeeServiceProxy, EmployeeDto } from '@shared/service-proxies/service-proxies';
import { Component, OnInit, Injector, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { AppAuthService } from '@shared/auth/app-auth.service';

@Component({
    templateUrl: './sidebar-user-area.component.html',
    selector: 'sidebar-user-area',
    encapsulation: ViewEncapsulation.None,
    providers: [DepartmentServiceProxy, EmployeeServiceProxy]
})
export class SideBarUserAreaComponent extends AppComponentBase implements OnInit {

    shownLoginName = '';
    department: String;

    constructor(
        injector: Injector,
        private _authService: AppAuthService,
        private _deptService: DepartmentServiceProxy,
        private _empService: EmployeeServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.shownLoginName = this.appSession.getShownLoginName();
        this._empService.getDetail(abp.session.userId).subscribe((reply: EmployeeDto) => {
            if (!reply.departmentId) {
                return;
            }
            this._deptService.getDetail(reply.departmentId).subscribe(dept => {
                this.department = dept.department.deptName;
            });
        });
    }

    logout(): void {
        this._authService.logout();
    }
}
