import {
  TenantServiceProxy,
  TenantDto} from '../service-proxies/service-proxies';
import { Component, ViewEncapsulation, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialog } from '@angular/material';
import { CreateTenantDialogComponent } from 'host/tenants/create-tenant/create-tenant-dialog.component';
import { EditTenantDialogComponent } from 'host/tenants/edit-tenant/edit-tenant-dialog.component';

@Component({
  selector: 'org-sidebar',
  templateUrl: './org-sidebar.component.html',
  encapsulation: ViewEncapsulation.None
})
export class OrgSidebarComponent extends AppComponentBase implements OnInit {
  tenant: TenantDto;
  tenantId = abp.multiTenancy.getTenantIdCookie();
  items = {
    selected: 0,
    other: 0
  };

  constructor(
    private injector: Injector,
    private _tenantService: TenantServiceProxy,
    private _dialog: MatDialog
    ) { super(injector); }

  ngOnInit(): void {
    this.items.selected += this.tenantId ? 1 : 0;

    if (!this.tenantId) { return; }
    this._tenantService.get(this.tenantId)
    .subscribe((result: TenantDto) => {
      this.tenant = result;
    });
  }

  createTenant(): void {
    this.showCreateOrEditTenantDialog();
}

editTenant(tenant: TenantDto): void {
    this.showCreateOrEditTenantDialog(tenant.id);
}


  showCreateOrEditTenantDialog(id?: number): void {
    let createOrEditTenantDialog;
    if (id === undefined || id <= 0) {
        createOrEditTenantDialog = this._dialog.open(CreateTenantDialogComponent);
    } else {
        createOrEditTenantDialog = this._dialog.open(EditTenantDialogComponent, {
            data: id
        });
    }

    createOrEditTenantDialog.afterClosed().subscribe(result => {
        if (result) {
            // this.refresh();
        }
    });
}
}
