import { AccountServiceProxy } from '@shared/service-proxies/service-proxies';
import {
    Injectable
} from '@angular/core';
import {
    AppConsts
} from '@shared/AppConsts';

@Injectable()
export class AppAuthService {
    constructor(private _accountService: AccountServiceProxy){
     }

    logout(reload ?: boolean): void {
        abp.auth.clearToken();
        abp.utils.setCookieValue(AppConsts.authorization.encryptedAuthTokenName, undefined, undefined, abp.appPath);
        if (reload !== false) {
            location.href = AppConsts.appBaseUrl;
        }
    }
}
