import { AppConsts } from '@shared/AppConsts';
import { Injectable } from '@angular/core';
import { PermissionCheckerService } from '@abp/auth/permission-checker.service';
import { AppSessionService } from '../session/app-session.service';

import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    CanActivateChild
} from '@angular/router';
import { AppAuthService } from './app-auth.service';

@Injectable()
export class AppRouteGuard implements CanActivate, CanActivateChild {
    tenantId = abp.multiTenancy.getTenantIdCookie();

    constructor(
        private _permissionChecker: PermissionCheckerService,
        private _router: Router,
        private _sessionService: AppSessionService,
        private _authService: AppAuthService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (!this._sessionService.user) {
            this._router.navigate(['/account/login']);
            return false;
        }

        if ((!route.data || !route.data['permission']) && this.tenantId) {
            return true;
        }

        if (this._permissionChecker.isGranted(route.data['permission'])) {
            return true;
        }

        const location = this.selectBestRoute();
        if (!location) {
            return false;
        }

        this._router.navigate([location]);
        return false;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

    selectBestRoute(): string {
        if (!this._sessionService.user) {
            return '/account/login';
        }

        if (this._permissionChecker.isGranted('Pages.Users')) {
            return '/host/users';
        }

        if (this._permissionChecker.isGranted('Pages.Tenants')) {
            return '/org/tenants';
        }

        if (!this.tenantId) {
            this._authService.logout();
            return;
        }
        return AppConsts.homeRoute;
    }
}
