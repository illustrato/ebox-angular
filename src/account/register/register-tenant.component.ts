import { Component,  AfterViewInit, Injector, ViewChild, ElementRef } from '@angular/core';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/app-component-base';
import { Router } from '@angular/router';
import { LoginService } from 'account/login/login.service';
import { TenantServiceProxy, CreateTenantDto, TenantDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './register-tenant.component.html',
  animations: [accountModuleAnimation()],
  providers: [TenantServiceProxy]
})
export class RegisterTenantComponent extends AppComponentBase implements AfterViewInit {

  @ViewChild('cardBody', {static: false}) cardBody: ElementRef;

  saving = false;
  tenant: CreateTenantDto = new CreateTenantDto();

  constructor(
    injector: Injector,
    private _router: Router,
    public _loginService: LoginService,
    private _tenantService: TenantServiceProxy) {
    super(injector);
  }

  ngAfterViewInit(): void {
    // $(this.cardBody.nativeElement).find('input:first').focus();
    this.tenant.init({ isActive: true });
  }
  save(): void {console.log(this.tenant);
    this.saving = true;
    this._tenantService.create(this.tenant)
    .pipe(finalize(() => { this.saving = false; }))
    .subscribe((result: TenantDto) => {console.log(this.tenant); console.log(result);
      this.notify.info(this.l('SavedSuccessfully'));

      // Autheticate
      this.saving = true;
      abp.multiTenancy.setTenantIdCookie(result.id);
      this._loginService.authenticateModel.userNameOrEmailAddress = this.tenant.adminEmailAddress;
      this._loginService.authenticateModel.password = this.tenant.password;
      this._loginService.authenticate(() => { this.saving = false; });
    });
  }
}
