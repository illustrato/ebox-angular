import { Component, Injector, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AbpSessionService } from '@abp/session/abp-session.service';
import { AppComponentBase } from '@shared/app-component-base';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
  animations: [accountModuleAnimation()]
})
export class LoginComponent extends AppComponentBase implements OnInit {

  @ViewChild('cardBody', {static: false}) cardBody: ElementRef;

  submitting = false;
  tenancyName: string;

  constructor(
    injector: Injector,
    private _router: Router,
    public loginService: LoginService,
    private _sessionService: AbpSessionService
  ) {
    super(injector);
  }
  get multiTenancySideIsTenant(): boolean {
    return this._sessionService.tenantId > 0;
}
  ngOnInit(): void {
    if (this.appSession.tenant) {
      this.tenancyName = this.appSession.tenant.tenancyName;
    }
  }

  get multiTenancySideIsTeanant(): boolean {
    return this._sessionService.tenantId > 0;
  }

  get isSelfRegistrationAllowed(): boolean {
    if (!this._sessionService.tenantId) {
      return false;
    }

    return true;
  }
  redirectToRegisterTenant(): void {
    const tenantId = abp.multiTenancy.getTenantIdCookie();
    if (tenantId) {
        window.location.href = 'account/register-tenant?TenantId=0';
    } else {
        this._router.navigate(['account/register-tenant']);
    }
}

  login(): void {
    this.submitting = true;
    this.loginService.authenticate(() => (this.submitting = false));
  }
}
