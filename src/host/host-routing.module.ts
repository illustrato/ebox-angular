import { HostComponent } from './host.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TenantsComponent } from './tenants/tenants.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { UsersComponent } from './users/users.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';


const routes: Routes = [{
  path: '',
  component: HostComponent,
  children: [
    { path: '', redirectTo: '/host/tenants', pathMatch: 'full' },
    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' }, canActivate: [AppRouteGuard] },
    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Users' }, canActivate: [AppRouteGuard] },
    { path: 'update-password', component: ChangePasswordComponent },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HostRoutingModule { }
