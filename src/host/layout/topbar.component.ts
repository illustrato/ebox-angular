import { AppComponentBase } from '@shared/app-component-base';
import { Component, ViewEncapsulation, Injector } from '@angular/core';

@Component({
  selector: 'top-bar',
  templateUrl: './topbar.component.html',
  encapsulation: ViewEncapsulation.None
})
export class TopbarComponent extends AppComponentBase {
  tenantId = abp.multiTenancy.getTenantIdCookie();
  constructor(injector: Injector) {
    super(injector);
    console.log(`tenant: ${this.tenantId}`);
    console.log(`user: ${abp.session.userId}`);
    console.log(`user-tenant: ${abp.session.tenantId}`);
   }

}
