import { AppComponentBase } from 'shared/app-component-base';
import { Component, ViewEncapsulation, Injector } from '@angular/core';
import { MenuItem } from '@shared/layout/menu-item';

@Component({
  selector: 'sidebar-nav',
  templateUrl: './sidebar-nav.component.html',
  encapsulation: ViewEncapsulation.None
})
export class SidebarNavComponent extends AppComponentBase {
  menuItems: MenuItem[] = [
    new MenuItem(this.l('Organizations'), 'Pages.Tenants', 'business', '/host/tenants'),
    new MenuItem(this.l('Users'), 'Pages.Users', 'people', '/host/users')
];
  constructor(injector: Injector) { super(injector); }


  showMenuItem(menuItem): boolean {
    if (menuItem.permissionName) {
        return this.permission.isGranted(menuItem.permissionName);
    }

    return true;
}

}
