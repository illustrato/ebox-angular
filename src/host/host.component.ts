import { Component, OnInit, Injector, ViewContainerRef, AfterViewInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { SignalRAspNetCoreHelper } from '@shared/helpers/SignalRAspNetCoreHelper';

@Component({
  templateUrl: './host.component.html'
})
export class HostComponent extends AppComponentBase implements OnInit, AfterViewInit {

  private viewContainerRef: ViewContainerRef;

  constructor(injector: Injector) { super(injector); }

  ngOnInit() {
    SignalRAspNetCoreHelper.initSignalR();
  }
  ngAfterViewInit(): void {
    $.AdminBSB.activateAll();
    $.AdminBSB.activateDemo();
  }
  onResize(event) {
    // exported from $.AdminBSB.activateAll
    $.AdminBSB.leftSideBar.setMenuHeight();
    $.AdminBSB.leftSideBar.checkStatuForResize(false);

    // exported from $.AdminBSB.activateDemo
    $.AdminBSB.demo.setSkinListHeightAndScroll();
    $.AdminBSB.demo.setSettingListHeightAndScroll();

    $.AdminBSB.demo.setBusinessListHeightAndScroll();
    $.AdminBSB.demo.setRolesListHeightAndScroll();
}
}
