import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HostRoutingModule } from './host-routing.module';
import { HostComponent } from './host.component';
import { SidebarNavComponent } from './layout/sidebar-nav.component';
import { TopbarComponent } from './layout/topbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { AbpModule } from 'abp-ng2-module/dist/src/abp.module';
import { SharedModule } from '@shared/shared.module';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { ModalModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { TenantsComponent } from './tenants/tenants.component';
import { CreateTenantDialogComponent } from './tenants/create-tenant/create-tenant-dialog.component';
import { EditTenantDialogComponent } from './tenants/edit-tenant/edit-tenant-dialog.component';
import { ResetPasswordDialogComponent } from './users/reset-password/reset-password.component';
import { EditUserDialogComponent } from './users/edit-user/edit-user-dialog.component';
import { CreateUserDialogComponent } from './users/create-user/create-user-dialog.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { UsersComponent } from './users/users.component';


@NgModule({
  declarations: [
    HostComponent,
    SidebarNavComponent,
    TopbarComponent,
    // tenants
    TenantsComponent,
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // users
    UsersComponent,
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ChangePasswordComponent,
    ResetPasswordDialogComponent,
  ],
  imports: [
    CommonModule,
    HostRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    AbpModule,
    SharedModule,
    ServiceProxyModule,
    ModalModule.forRoot(),
    NgxPaginationModule
  ],
  entryComponents:[
    // tenants
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // users
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ResetPasswordDialogComponent,
  ]
})
export class HostModule { }
