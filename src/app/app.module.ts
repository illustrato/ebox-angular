import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AbpModule } from '@abp/abp.module';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';

import { HomeComponent } from '@app/home/home.component';
import { AboutComponent } from '@app/about/about.component';
import { TopBarComponent } from '@app/layout/topbar.component';
import { SideBarNavComponent } from '@app/layout/sidebar-nav.component';
import { ItemsComponent } from './items/items.component';
import { ItemDetailComponent } from './items/item-detail/item-detail.component';
import { DmlItemComponent } from './items/dml-item/dml-item.component';

import { ImageCropperModule } from 'ngx-image-cropper';
import { UnitComponent } from './items/dml-item/unit.component';
import { DelegationComponent } from './items/item-detail/delegation.component';
import { RepositoryComponent } from './repository/repository.component';
import { ItemViewComponent } from './repository/item-view/item-view.component';
import { LogFaultComponent } from './repository/log-fault/log-fault.component';
import { AcquisitionsComponent } from './acquisitions/acquisitions.component';
import { YourRequestsComponent } from './requests/your-requests.component';
import { StaffRequestsComponent } from './requests/staff-requests.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    TopBarComponent,
    SideBarNavComponent,
    // Equipment
    ItemsComponent,
    ItemDetailComponent,
    DmlItemComponent,
    UnitComponent,
    DelegationComponent,
    RepositoryComponent,
    ItemViewComponent,
    LogFaultComponent,
    AcquisitionsComponent,
    YourRequestsComponent,
    StaffRequestsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forRoot(),
    AbpModule,
    AppRoutingModule,
    ServiceProxyModule,
    SharedModule,
    NgxPaginationModule,
    ImageCropperModule
  ],
  providers: [],
  entryComponents: [
    // Equipment
    DmlItemComponent,
    UnitComponent,
    DelegationComponent,
    LogFaultComponent
  ]
})
export class AppModule {}
