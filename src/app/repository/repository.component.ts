import {
  EquipmentServiceProxy,
  EmployeeServiceProxy,
  UnitOutputDto,
  ListResultDtoOfUnitOutputDto,
  ItemTypeListDto
} from '@shared/service-proxies/service-proxies';
import { Component, Injector } from '@angular/core';
import { PagedListingComponentBase, PagedRequestDto } from '@shared/paged-listing-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { finalize } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { LogFaultComponent } from './log-fault/log-fault.component';

class PagedUnitsRequestDto extends PagedRequestDto {
  keyword: string;
  typeId: number | null;
}

@Component({
  templateUrl: './repository.component.html',
  animations: [appModuleAnimation()],
  styles: [
    `
        mat-form-field {
          padding: 10px;
        }
      `
  ],
  providers: [EquipmentServiceProxy, EmployeeServiceProxy]
})
export class RepositoryComponent extends PagedListingComponentBase<UnitOutputDto>  {
  units: UnitOutputDto[] = [];
  types: ItemTypeListDto[] = [];
  keyword = '';
  typeId: number | null;

  constructor(
    injector: Injector,
    private _itemService: EquipmentServiceProxy,
    private _empService: EmployeeServiceProxy,
    private _dialog: MatDialog
  ) { super(injector); }

  searchPhrase(e) {
    if (e.keyCode === 27) {
      e.srcElement.value = this.keyword = '';
      e.srcElement.blur();
    }
    this.refresh();
  }
  returnItem(item: UnitOutputDto) {
    abp.message.confirm(
      this.l(`you are about to return item : ${item.equipment.title}`, item.equipment.title),
      (result: boolean) => {
        if (result) {
          // this._equipmentService.delete(this.itemId).subscribe(() => {
          //   abp.notify.success(this.l('Successfully Deleted'));
          //   this.refresh();
          // });
        }
      }
    );
  }
  logFault(item: UnitOutputDto) {
    this.showLogFaulttDialog(item);
  }

  protected list(request: PagedUnitsRequestDto, pageNumber: number, finishedCallback: Function): void {

    request.keyword = this.keyword;
    request.typeId = this.typeId;

    this._itemService.getTypes().subscribe(types => {
      this.types = types.items;
    });
    this._empService.getEquipment(request.keyword, request.typeId, abp.session.userId)
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: ListResultDtoOfUnitOutputDto) => {
        this.units = result.items;
      });
  }
  protected delete(entity: UnitOutputDto): void {
  }

  private showLogFaulttDialog(target: UnitOutputDto): void {
    let logFaultDialog;
    logFaultDialog = this._dialog.open(LogFaultComponent, {
      data: target
    });

    logFaultDialog.afterClosed().subscribe(result => {
      if (result) {
        this.refresh();
      }
    });

  }
}
