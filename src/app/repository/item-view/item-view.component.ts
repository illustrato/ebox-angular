import { Moment } from 'moment';
import { Component, OnInit, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { UnitServiceProxy, UnitOutputDto, EquipmentTypeDto, SampleServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { ActivatedRoute, Params } from '@angular/router';
import * as moment from 'moment';
import { MatDialog } from '@angular/material';
import { LogFaultComponent } from '../log-fault/log-fault.component';

@Component({
  templateUrl: './item-view.component.html',
  animations: [appModuleAnimation()],
  providers: [UnitServiceProxy, SampleServiceProxy]
})
export class ItemViewComponent extends AppComponentBase implements OnInit {

  item: UnitOutputDto = new UnitOutputDto();
  itemType: EquipmentTypeDto = new EquipmentTypeDto();
  itemId: number;
  title: string;
  description: string;
  specs: string;
  itemPhoto = '../../../assets/images/logo.png';
  duration: string;

  constructor(
    injector: Injector,
    private _unitService: UnitServiceProxy,
    private _sampleService: SampleServiceProxy,
    private _dialog: MatDialog,
    private _activatedRoute: ActivatedRoute) { super(injector); }

  ngOnInit() {
    this._activatedRoute.params.subscribe((params: Params) => {
      this.itemId = params['unitId'];
      this.loadIntel();
    });
  }

  loadIntel() {
    this._unitService.getDetail(this.itemId).subscribe((result: UnitOutputDto) => {
      this.item = result;
      this.title = result.equipment.title;
      this.description = result.equipment.description;
      this.specs = result.equipment.specs;
      this.itemType = result.equipment.equipmentType;
      this.duration = `You've had item for ${result.usageDate.fromNow(true)} now.`;

      if (result.equipment.image) {
        this._sampleService.getBase64(`wwwroot/img/equipment/${abp.session.tenantId}/${result.equipment.id}.png`).subscribe((base64) => {
          this.itemPhoto = base64.id;
        });
      }
    });
  }

  returnItem() {
    abp.message.confirm(
      this.l(`you are about to return item : ${this.title}`, this.title),
      (result: boolean) => {
        if (result) {
          // this._equipmentService.delete(this.itemId).subscribe(() => {
          //   abp.notify.success(this.l('Successfully Deleted'));
          //   this.refresh();
          // });
        }
      }
    );
  }

  logFault() {
    this.showLogFaulttDialog(this.item);
  }

  private showLogFaulttDialog(target: UnitOutputDto): void {
    let logFaultDialog;
    logFaultDialog = this._dialog.open(LogFaultComponent, {
      data: target
    });

    logFaultDialog.afterClosed().subscribe( result => {
      if (result) {
        this.loadIntel();
      }
    });

  }

}
