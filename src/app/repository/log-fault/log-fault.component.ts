import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { UnitOutputDto, UnitServiceProxy, Fault, SampleServiceProxy } from '@shared/service-proxies/service-proxies';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  templateUrl: './log-fault.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
      mat-checkbox {
        padding-bottom: 5px;
      }
    `
  ],
  providers: [UnitServiceProxy, SampleServiceProxy]
})
export class LogFaultComponent extends AppComponentBase implements OnInit {
  saving = false;
  item: UnitOutputDto = new UnitOutputDto();
  fault: Fault = new Fault();

  title: string;
  image = '../../../assets/images/logo.png';

  constructor(injector: Injector,
    private _unitService: UnitServiceProxy,
    private _sampleService: SampleServiceProxy,
    private _dialogRef: MatDialogRef<LogFaultComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public _target: UnitOutputDto) { super(injector); }

  ngOnInit() {
    if (this._target.equipment.image) {
      // tslint:disable-next-line: max-line-length
      this._sampleService.getBase64(`wwwroot/img/equipment/${abp.session.tenantId}/${this._target.equipment.id}.png`).subscribe((base64) => {
        this.image = base64.id;
      });
    }
  }

  save(): void {

  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
