import { Component, Injector, ViewEncapsulation, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { TenantServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './topbar.component.html',
    selector: 'top-bar',
    encapsulation: ViewEncapsulation.None
})
export class TopBarComponent extends AppComponentBase implements OnInit {
    public keyword: string;
    tenantId = abp.multiTenancy.getTenantIdCookie();
    tenancyName = 'Business';
    constructor(
        injector: Injector,
        private _tenantService: TenantServiceProxy
    ) {
        super(injector);
        console.log(`tenant: ${this.tenantId}`);
        console.log(`user: ${abp.session.userId}`);
        console.log(`user-tenant: ${abp.session.tenantId}`);
    }
    ngOnInit(): void {
        this._tenantService.get(this.tenantId)
        .subscribe(result => {
            this.tenancyName = result.tenancyName;
        });
    }
}
