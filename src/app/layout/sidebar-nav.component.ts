import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MenuItem } from '@shared/layout/menu-item';

@Component({
    templateUrl: './sidebar-nav.component.html',
    selector: 'sidebar-nav',
    encapsulation: ViewEncapsulation.None
})
export class SideBarNavComponent extends AppComponentBase {

    menuItems: MenuItem[] = [
        new MenuItem(this.l('HomePage'), '', 'home', '/app/home'),
        new MenuItem(this.l('Equipment'), 'Pages.Equipment', 'dns', '/app/items'),
        new MenuItem(this.l('Staff Requests'), 'Pages.Staff_Requests', 'assignment', '/app/staffrequests'),
        new MenuItem(this.l('Repository'), 'Pages.Repo', 'archive', '/app/repo'),
        new MenuItem(this.l('Acquire Items'), 'Pages.Acquisitions', 'queue', '/app/acquisitions'),
        new MenuItem(this.l('Your Requests'), 'Pages.Your_Requests', 'library_books', '/app/yourrequests')
    ];

    constructor(
        injector: Injector
    ) {
        super(injector);
    }

    showMenuItem(menuItem): boolean {
        if (menuItem.permissionName) {
            return this.permission.isGranted(menuItem.permissionName);
        }

        return true;
    }
}
