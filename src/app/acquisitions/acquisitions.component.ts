import {
  EmployeeServiceProxy,
  EquipmentDto,
  PagedResultDtoOfEquipmentDto,
  ItemTypeListDto,
  ListResultDtoOfItemTypeListDto,
  EquipmentServiceProxy,
  EmployeeDetailOutputDto,
  UnitDto,
  UnitServiceProxy,
  ListResultDtoOfUnitOutputDto,
  PagedResultDtoOfUnitDto
} from '@shared/service-proxies/service-proxies';

import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { PagedListingComponentBase, PagedRequestDto } from '@shared/paged-listing-component-base';
import { finalize } from 'rxjs/operators';

class PagedItemsRequestDto extends PagedRequestDto {
  keyword: string;
  typeId: number | null;
}

class PagedUnitsRequestDto extends PagedRequestDto {
  keyword: string;
  onlyFit: boolean;
}

@Component({
  templateUrl: './acquisitions.component.html',
  animations: [appModuleAnimation()],
  providers: [EquipmentServiceProxy, EmployeeServiceProxy, UnitServiceProxy],
  styles: [
    `
      .card {
        margin-bottom: 10px;
      }
      .card .header.custom {
        padding: 5px 20px
      }
      mat-form-field > div {
        border-bottom: 1px solid #673AB7!important
      }
      .head-text{
        color: #673AB7
      }
      .mat-icon-button, .mat-icon-button {
        line-height: unset;
        height: auto
      }
    `
  ]
})
export class AcquisitionsComponent extends PagedListingComponentBase<EquipmentDto> implements OnInit {

  active = false;
  items: EquipmentDto[] = [];
  departmentId = '';
  itemId: string;

  types: ItemTypeListDto[] = [];
  keyword = '';
  typeId: number | null;

  serial = '';
  units: UnitDto[] = [];

  constructor(injector: Injector,
    private _itemService: EquipmentServiceProxy,
    private _empService: EmployeeServiceProxy,
    private _unitService: UnitServiceProxy) {
    super(injector);
    this.pageSize = 3;
  }

  ngOnInit() {
    this._empService.getDetail(abp.session.userId).subscribe((emp: EmployeeDetailOutputDto) => {
      this.departmentId = emp.departmentId;
      this.refresh();
    });
    this._itemService.getTypes().subscribe((result: ListResultDtoOfItemTypeListDto) => {
      this.types = result.items;
    });
  }
  searchPhrase(e) {
    if (e.keyCode === 27) {
      e.srcElement.value = this.keyword = '';
      e.srcElement.blur();
    }
    this.refresh();
  }
  filterUnits(e){
  }

  selectItem(id: string) {
    const target = $(`#heard_${id}`);
    this.ctrlBatch($('.header.item'), this.backgroundHeaders);
    this.ctrlBatch($('.btn.ls-units'), this.enableButtons);
    target.removeClass('dim');
    target.addClass('selected');

    const btn = document.getElementById(`btn_${id}`);
    btn.setAttribute('disabled', 'true');

    const request = new PagedUnitsRequestDto();
    request.keyword = this.serial;
    request.onlyFit = false;

    this._unitService.getAll(request.keyword, request.onlyFit, id, request.skipCount, request.maxResultCount)
    .subscribe((result: PagedResultDtoOfUnitDto) => {
      this.itemId = id;
      this.units = result.items;
    });
  }

  makeRequest(unit: UnitDto): void {
    console.log(unit);
  }

  backgroundHeaders(index: number, ctrl: Element) {
      //  ctrl.className = 'header item dim';
       ctrl.classList.add('dim');
       ctrl.classList.remove('selected');
  }

  enableButtons(index: number, ctrl: Element) {
    ctrl.removeAttribute('disabled');
  }

  ctrlBatch(controls: JQuery<Element>, fx: (index: number, ctrl: Element) => void) {
    controls.each((index, ctrl) => {
      fx(index, ctrl);
    });
  }

  closeSelection() {
    this.refresh();
  }

  protected list(request: PagedItemsRequestDto, pageNumber: number, finishedCallback: Function): void {
    request = new PagedItemsRequestDto();
    request.keyword = this.keyword;
    request.typeId = this.typeId;

    this._itemService.getAll(request.keyword, false, request.typeId, this.departmentId, request.skipCount, request.maxResultCount)
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: PagedResultDtoOfEquipmentDto) => {
        this.items = result.items;
        this.showPaging(result, pageNumber);
        this.itemId = null;
      });
  }
  protected delete(entity: EquipmentDto): void {
  }

}
