import { DelegateInputDto, EntityDtoOfUInt64 } from './../../../shared/service-proxies/service-proxies';
import { Moment } from 'moment';
import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import {
  UnitServiceProxy,
  EmployeeServiceProxy,
  UnitOutputDto,
  SampleServiceProxy,
  EmployeeDto,
  PagedResultDtoOfEmployeeDto,
  DepartmentServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './delegation.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
      mat-checkbox {
        padding-bottom: 5px;
      }
      image-cropper {
        max-height: 320px;
      }
    `
  ],
  providers: [UnitServiceProxy, EmployeeServiceProxy, SampleServiceProxy, DepartmentServiceProxy]
})
export class DelegationComponent extends AppComponentBase implements OnInit {
  saving = false;
  item: UnitOutputDto = new UnitOutputDto();

  title: string;
  image = '../../../assets/images/logo.png';
  serial: string;
  userid: number;
  good: boolean;
  captured: Moment;

  department: string;
  staff: EmployeeDto[] = [];
  employee: EmployeeDto = new EmployeeDto();
  empName: string;
  idNo: string;
  email: string;
  phone: string;
  using: Moment;


  constructor(injector: Injector,
    private _unitSerive: UnitServiceProxy,
    private _empService: EmployeeServiceProxy,
    private _sanpleService: SampleServiceProxy,
    private _departmentService: DepartmentServiceProxy,
    private _dialogRef: MatDialogRef<DelegationComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public _id: number) { super(injector); }

  ngOnInit() {
    this._unitSerive.getDetail(this._id).subscribe((result: UnitOutputDto) => {
      this.title = result.equipment.title;
      this.serial = result.productSerial;
      this.userid = result.userId;
      this.good = result.isFit;
      this.captured = result.date;



      if (result.equipment.image) {
        this._sanpleService.getBase64(`wwwroot/img/equipment/${abp.session.tenantId}/${result.equipment.id}.png`).subscribe(base64 => {
          this.image = base64.id;
        });
        this._departmentService.get(result.equipment.departmentId).subscribe(dept => {
          this.department = dept.department.deptName;
        });

      }
      if (!result.isFit) {
        return;
      }
      if (result.userId) {
        this._empService.get(result.userId).subscribe(emp => {
          this.employee = emp;
          this.empName = emp.user.fullName;
          this.idNo = emp.idNo;
          this.email = emp.user.emailAddress;
          this.phone = emp.user.phoneNumber;
          this.using = result.usageDate;
        });
      } else {
        this._empService
          .getAll('', true, result.equipment.departmentId, 0, 50)
          .subscribe((employees: PagedResultDtoOfEmployeeDto) => {
            this.staff = employees.items;
          });
      }
    });
  }

  save(): void {

  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

  delegate(emp: EmployeeDto): void {
    this.saving = true;
    const candidate: DelegateInputDto = new DelegateInputDto({
      userId: emp.id,
      unitId: this._id
    });
    this._unitSerive.delegate(candidate)
    .pipe(
      finalize(() => {
        this.saving = false;
      })
    ).subscribe(() => {
      this.close(true);
    });
  }

  revoke(): void {
    this.saving = true;
    this._unitSerive.revoke(new EntityDtoOfUInt64({id: this._id}))
    .pipe(
      finalize(() => {
        this.saving = false;
      })
    )
    .subscribe(() =>{
      this.close(true);
    });
  }
}
