import { Component, OnInit, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  EquipmentServiceProxy,
  EquipmentOutputDto,
  UnitDto,
  DepartmentServiceProxy,
  SampleServiceProxy,
  UnitServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog } from '@angular/material';
import { DmlItemComponent } from '../dml-item/dml-item.component';
import { UnitComponent } from '../dml-item/unit.component';
import { DelegationComponent } from './delegation.component';

@Component({
  templateUrl: './item-detail.component.html',
  animations: [appModuleAnimation()],
  providers: [EquipmentServiceProxy, DepartmentServiceProxy, SampleServiceProxy, UnitServiceProxy]
})
export class ItemDetailComponent extends AppComponentBase implements OnInit {

  item: EquipmentOutputDto = new EquipmentOutputDto();
  itemName: string;
  itemIcon: string;
  units: UnitDto[] = [];
  itemId: string;

  logo = '../../../assets/images/logo.png';
  itemPhoto: string;
  itemType: string;
  itemDept: string;

  constructor(
    private injector: Injector,
    private _equipmentService: EquipmentServiceProxy,
    private _unitService: UnitServiceProxy,
    private _departmentService: DepartmentServiceProxy,
    private _sampleService: SampleServiceProxy,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _dialog: MatDialog) {
    super(injector);
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe((params: Params) => {
      this.itemId = params['itemId'];
      this.loadEquipment();
    });
  }
  loadEquipment() {
    this._equipmentService.getDetail(this.itemId)
      .subscribe((result: EquipmentOutputDto) => {
        this.item = result;
        this.itemIcon = result.equipmentType.fontIcon;
        this.units = result.units;
        this.itemType = result.equipmentType.title;
        this._departmentService.getDetail(result.departmentId).subscribe(department => { this.itemDept = department.department.deptName; });

        if (result.image) {
          this._sampleService.getBase64(`wwwroot/img/equipment/${abp.session.tenantId}/${result.id}.png`).subscribe((base64) => {
            this.itemPhoto = base64.id;
          });
        } else {
          this.itemPhoto = this.logo;
        }
      });
  }
  backToEquipmentPage() {
    this._router.navigate(['app/items']);
  }

  refresh() {
    this.loadEquipment();
  }
  updateItem(): void {
    const createOrEditItemDialog = this._dialog.open(DmlItemComponent, {
      data: this.item
    });
    createOrEditItemDialog.afterClosed().subscribe(result => {
      if (result) {
        this.loadEquipment();
      }
    });
  }

  deleteItem(): void {
    abp.message.confirm(
      this.l(`you are about to delete item : ${this.item.title}`, this.item.title),
      (result: boolean) => {
        if (result) {
          this._equipmentService.delete(this.itemId).subscribe(() => {
            abp.notify.success(this.l('Successfully Deleted'));
            this.backToEquipmentPage();
          });
        }
      }
    );
  }

  addUnit(): void {
    const createOrEditUnitDialog = this._dialog.open(UnitComponent, {
      data: this.itemId
    });
    createOrEditUnitDialog.afterClosed().subscribe(result => {
      if (result) {
        this.refresh();
      }
    });
  }
  deleteUnit(unit: UnitDto): void {
    abp.message.confirm(
      this.l('UserDeleteWarningMessage', unit.productSerial),
      (result: boolean) => {
        if (result) {
          this._unitService.delete(unit.id).subscribe(() => {
            abp.notify.success(this.l('Successfully Deleted'));
            this.refresh();
          });
        }
      }
    );
  }

  toggleCondition(unit: UnitDto): void {
    this._unitService.fit(unit).subscribe(() => {
      abp.notify.success(this.l('Condition Toggled'));
      this.refresh();
    });
  }

  itemUsage(unit: UnitDto): void {
    const itemUsageDialog = this._dialog.open(DelegationComponent, {
      data: unit.id
    });
    itemUsageDialog.afterClosed().subscribe(result => {
      if (result) {
        this.refresh();
      }
    });
  }

}
