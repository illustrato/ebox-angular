import {
  PagedResultDtoOfEquipmentDto,
  EquipmentServiceProxy,
  EquipmentDto,
  ItemTypeListDto,
  DepartmentServiceProxy,
  OrgDeptListDto
} from '@shared/service-proxies/service-proxies';
import { Component, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { PagedListingComponentBase, PagedRequestDto } from '@shared/paged-listing-component-base';
import { MatDialog } from '@angular/material';
import { DmlItemComponent } from './dml-item/dml-item.component';

class PagedItemRequestDto extends PagedRequestDto {
  keyword: string;
  includeDeleted: boolean | null;
  filterType: number | null;
  filterDept: string | null;
}

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  animations: [appModuleAnimation()],
  providers: [EquipmentServiceProxy, DepartmentServiceProxy]
})
export class ItemsComponent extends PagedListingComponentBase<EquipmentDto> {

  // @Input() searchPhrase: string;

  active = false;
  equipment: EquipmentDto[] = [];
  itemTypes: ItemTypeListDto[] = [];
  departments: OrgDeptListDto[] = [];

  // public pageNumber = 1;


  //  filters
  filter: PagedItemRequestDto = new PagedItemRequestDto();

  constructor(
    injector: Injector,
    private _equipmentService: EquipmentServiceProxy,
    private _departmentService: DepartmentServiceProxy,
    private _dialog: MatDialog
  ) {
    super(injector);

    this.pageSize = 4;
    $('.search-bar').find('input[type="text"]').on('keyup', (e) => { 
      if (e.keyCode === 27) {
        document.getElementById('txtTopbarSearch').setAttribute('ng-reflect-model', '');
      }
      this.loadEquipment();
    });
    $('.search-bar').find('.close-search').on('click', () => {
      document.getElementById('txtTopbarSearch').setAttribute('ng-reflect-model', '');
      this.loadEquipment();
    });
  }

  loadEquipment(pageNumber?: number) {
    const keyword = document.getElementById('txtTopbarSearch').getAttribute('ng-reflect-model');
    this._equipmentService.getAll(
      keyword,
      this.filter.includeDeleted,
      this.filter.filterType,
      this.filter.filterDept,
      this.filter.skipCount,
      this.filter.maxResultCount)
      .subscribe((result: PagedResultDtoOfEquipmentDto) => {
        this.equipment = result.items;
        this.showPaging(result, pageNumber);
      });
    this._equipmentService.getTypes().subscribe(types => {
      this.itemTypes = types.items;
    });
    this._departmentService.getAssociation(false).subscribe(result => {
      this.departments = result.items;
    });
  }
  // Show Modals
  createItem(): void {
    this.showCreateOrEditEmuipmentDialog();
  }

  editItem(item: EquipmentDto): void {
    this.showCreateOrEditEmuipmentDialog();
  }

  //  ----------------------------------------------------  Filters
  includeDeletedEquipmentCheckboxChanged() {
    this.loadEquipment();
  }
  itemTypeComboboxChanged() {
    this.loadEquipment();
  }
  departmentComboboxChanged() {
    this.loadEquipment();
  }

  protected list(request: PagedItemRequestDto, pageNumber: number, finishedCallback: Function): void {
    this.loadEquipment(pageNumber);
    finishedCallback();
  }

  protected delete(item: any): void {
    abp.message.confirm(
      'Are you sure you want to delete Equipment?',
      (result: boolean) => {
        if (result) {
          this._equipmentService.delete(item)
            .subscribe(() => {
              abp.notify.info('Equipment is deleted');
              this.refresh();
            });
        }
      }
    );
  }

  private showCreateOrEditEmuipmentDialog(id?: number): void {
    let createOrEditEmipmentpDialog;
    if (id === undefined || id <= 0) {
      createOrEditEmipmentpDialog = this._dialog.open(DmlItemComponent);
    } else {
      createOrEditEmipmentpDialog = this._dialog.open(DmlItemComponent, {
        data: id
      });
    }

    createOrEditEmipmentpDialog.afterClosed().subscribe(result => {
      if (result) {
        this.refresh();
      }
    });

  }

}
