import { Component, OnInit, Injector, Inject, Optional } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateUnitInput, UnitServiceProxy } from '@shared/service-proxies/service-proxies';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './unit.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
      mat-checkbox {
        padding-bottom: 5px;
      }
      image-cropper {
        max-height: 320px;
      }
    `
  ],
  providers: [UnitServiceProxy]
})
export class UnitComponent extends AppComponentBase implements OnInit {
  saving = false;
  unit: CreateUnitInput = new CreateUnitInput();

  constructor(
    injector: Injector,
    private _unitSerive: UnitServiceProxy,
    private _dialogRef: MatDialogRef<UnitComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public _id: string) {
    super(injector);
  }

  ngOnInit() {
    this.unit.equipmentId = this._id;
  }

  save(): void {
    this.saving = true;
    this._unitSerive
      .create(this.unit)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
