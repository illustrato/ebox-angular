import {
  ListResultDtoOfItemTypeListDto,
  Base64Dto,
  SampleServiceProxy,
  ItemTypeListDto,
  CreateEquipmentInput,
  ListResultDtoOfOrgDeptListDto,
  EquipmentServiceProxy,
  DepartmentServiceProxy,
  OrgDeptListDto, EquipmentDto, EquipmentOutputDto
} from '@shared/service-proxies/service-proxies';

import { Injector, Optional, Inject, OnInit, Component, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppComponentBase } from '@shared/app-component-base';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { finalize } from 'rxjs/operators';
@Component({
  templateUrl: './dml-item.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
      mat-checkbox {
        padding-bottom: 5px;
      }
      image-cropper {
        max-height: 320px;
      }
    `
  ],
  providers: [EquipmentServiceProxy, DepartmentServiceProxy, SampleServiceProxy]
})
export class DmlItemComponent extends AppComponentBase implements OnInit {
  saving = false;
  item: any;
  departments: OrgDeptListDto[] = [];
  types: ItemTypeListDto[] = [];
  logo = '../../../assets/images/logo.png';

  imageChangedEvent: any = '';
  croppedImage: any = this.logo;
  imageChanged = false;
  rm_img = false;

  @ViewChild(ImageCropperComponent, { static: false }) imageCropper: ImageCropperComponent;

  constructor(injector: Injector,
    public _equipmentService: EquipmentServiceProxy,
    private _dialogRef: MatDialogRef<DmlItemComponent>,
    private _deptService: DepartmentServiceProxy,
    private _sampleService: SampleServiceProxy,
    @Optional() @Inject(MAT_DIALOG_DATA) public _item: EquipmentOutputDto) {
    super(injector);
    if (this._item) {
      this.item = new EquipmentDto();
    } else {
      this.item = new CreateEquipmentInput();
    }
  }

  ngOnInit() {
    this._deptService.getAssociation(false).subscribe((result: ListResultDtoOfOrgDeptListDto) => {
      this.departments = result.items;
    });
    this._equipmentService.getTypes().subscribe((result: ListResultDtoOfItemTypeListDto) => {
      this.types = result.items;
    });
    if (!this._item) {
      return;
    }

    this.item = this._item;
    if (!this.item.image) {
      return;
    }
    this._sampleService.getBase64(`wwwroot/img/equipment/${abp.session.tenantId}/${this._item.id}.png`).subscribe(base64 => {
      this.croppedImage = base64.id;
    });
  }

  rmImage(): void {
    this.rm_img = this.imageChanged = true;
    this.croppedImage = this.logo;
    this.item.image = null;
  }

  save(): void {
    this.saving = true;
    if (!this._item) {
      this._equipmentService.create(this.item)
        .pipe(
          finalize(() => {
            this.saving = false;
          })
        )
        .subscribe((result?: EquipmentDto) => {
          this.notify.info(this.l('SavedSuccessfully'));
          if (!this.imageChanged) {
            this.close(true);
            return;
          }
          const img = new Base64Dto({ base64: this.croppedImage, insert: true });
          this.saving = true;
          this._equipmentService.setImage(result.id, img)
            .pipe(
              finalize(() => {
                this.saving = false;
              })
            )
            .subscribe(() => {
              this.close(true);
            });
        });
    } else {
      this._equipmentService.update(this.item)
        .pipe(
          finalize(() => {
            this.saving = false;
          })
        )
        .subscribe(() => {
          this.notify.info(this.l('SavedSuccessfully'));
          if (!this.imageChanged) {
            this.close(true);
            return;
          }
          const img = new Base64Dto({ base64: this.croppedImage, insert: !this.rm_img });
          this.saving = true;
          this._equipmentService.setImage(this._item.id, img)
            .pipe(
              finalize(() => {
                this.saving = false;
              })
            )
            .subscribe(() => {
              this.close(true);
            });
        });
    }
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.imageChanged = true;
    this.rm_img = false;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  rotateLeft() {
    this.imageCropper.rotateLeft();
  }
  rotateRight() {
    this.imageCropper.rotateRight();
  }
  flipHorizontal() {
    this.imageCropper.flipHorizontal();
  }
  flipVertical() {
    this.imageCropper.flipVertical();
  }

}
