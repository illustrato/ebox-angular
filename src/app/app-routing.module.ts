import { YourRequestsComponent } from './requests/your-requests.component';
import { AcquisitionsComponent } from './acquisitions/acquisitions.component';
import { RepositoryComponent } from './repository/repository.component';
import { ItemsComponent } from './items/items.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ItemDetailComponent } from './items/item-detail/item-detail.component';
import { ItemViewComponent } from './repository/item-view/item-view.component';
import { StaffRequestsComponent } from './requests/staff-requests.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                children: [
                    { path: '', redirectTo: '/app/home', pathMatch: 'full' },
                    { path: 'home', component: HomeComponent,  canActivate: [AppRouteGuard] },
                    // tslint:disable-next-line: max-line-length
                    { path: 'repo', component: RepositoryComponent, data: { permission: 'Pages.Repo' },  canActivate: [AppRouteGuard] },
                    // tslint:disable-next-line: max-line-length
                    { path: 'repo/:unitId', component: ItemViewComponent, data: { permission: 'Pages.Repo' },  canActivate: [AppRouteGuard] },
                    // tslint:disable-next-line: max-line-length
                    { path: 'staffrequests', component: StaffRequestsComponent, data: { permission: 'Pages.Staff_Requests' },  canActivate: [AppRouteGuard] },
                    // tslint:disable-next-line: max-line-length
                    { path: 'yourrequests', component: YourRequestsComponent, data: { permission: 'Pages.Your_Requests' },  canActivate: [AppRouteGuard] },
                    // tslint:disable-next-line: max-line-length
                    { path: 'acquisitions', component: AcquisitionsComponent, data: { permission: 'Pages.Acquisitions' },  canActivate: [AppRouteGuard] },
                    { path: 'about', component: AboutComponent },
                    { path: 'items', component: ItemsComponent, data: { permission: 'Pages.Equipment' }, canActivate: [AppRouteGuard] },
                    { path: 'items/:itemId', component: ItemDetailComponent, data: { permission: 'Pages.Equipment' } }
                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
