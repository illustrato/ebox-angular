import { NewItemsComponent } from './new-items/new-items.component';
import { StatsComponent } from './stats.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';


const routes: Routes = [
  {
    path: '',
    component: StatsComponent,
    children: [
      { path: '', redirectTo: '/stats/new', pathMatch: 'full'},
      { path: 'new', component: NewItemsComponent, data: { permission: 'Pages.Stats_New' }, canActivate: [AppRouteGuard]}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatsRoutingModule { }
