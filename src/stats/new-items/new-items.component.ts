import { Moment } from 'moment';
import { ListResultDtoOfNewItemsListDto, NewItemsListDto } from './../../shared/service-proxies/service-proxies';
import { Component, AfterViewInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { StatsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ChartType, ChartEvent } from 'ng-chartist';
import { IChartistData, IChartistAnimationOptions, ILineChartOptions } from 'chartist';

class NewItemsInputDto {
  start: Moment | null;
  end: Moment | null;
  typeId: number | null;
  departmentId: string | null;
}

@Component({
  templateUrl: './new-items.component.html',
  animations: [appModuleAnimation()],
  providers: [StatsServiceProxy]
})
export class NewItemsComponent extends AppComponentBase implements AfterViewInit {

  filter: NewItemsInputDto = new NewItemsInputDto();

  hits: NewItemsListDto[] = [];

  type: ChartType = 'Line';
  data: IChartistData;

  options: ILineChartOptions = {
    axisX: {labelOffset: {x: -35}},
    axisY: {onlyInteger: true},
    low: 0,
    height: 360,
    showArea: true,
    fullWidth: true,
    showPoint: false
  };

  events: ChartEvent = {
    draw: (data) => {
      if (data.type === 'bar') {
        data.element.animate({
          y2: <IChartistAnimationOptions>{
            dur: '0.5s',
            from: data.y1,
            to: data.y2,
            easing: 'easeOutQuad'
          }
        });
      }
    }
  };

  constructor(
    injector: Injector,
    private _statsService: StatsServiceProxy
  ) {
    super(injector);
  }

  ngAfterViewInit(): void {
    this._statsService.getNewItems(
      this.filter.start,
      this.filter.start,
      this.filter.typeId,
      this.filter.departmentId
    )
      .subscribe((result: ListResultDtoOfNewItemsListDto) => {
        this.hits = result.items;

        const days: string[] = [];
        const values: number[] = [];

        for (let i = 0 ; i < result.items.length ; i++) {
          days.push(this.hits[i].date.format('MMM Do'));
          values.push(this.hits[i].id);
        }
        const set: IChartistData = {
          labels: days,
          series: [values]
        };
        this.data = set;
      });
  }

}
