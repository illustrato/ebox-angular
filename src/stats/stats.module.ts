import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatsRoutingModule } from './stats-routing.module';
import { SidebarNavComponent } from './layout/sidebar-nav.component';
import { TopbarComponent } from './layout/topbar.component';
import { NewItemsComponent } from './new-items/new-items.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { AbpModule } from 'abp-ng2-module/dist/src/abp.module';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { StatsComponent } from './stats.component';
import { ChartistModule } from 'ng-chartist';


@NgModule({
  declarations: [
    StatsComponent,
    SidebarNavComponent,
    TopbarComponent,
    NewItemsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forRoot(),
    AbpModule,
    ServiceProxyModule,
    SharedModule,
    NgxPaginationModule,
    StatsRoutingModule,
    ChartistModule
  ]
})
export class StatsModule { }
