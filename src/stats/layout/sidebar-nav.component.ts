import { Component, OnInit, ViewEncapsulation, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MenuItem } from '@shared/layout/menu-item';

@Component({
  selector: 'sidebar-nav',
  templateUrl: './sidebar-nav.component.html',
  encapsulation: ViewEncapsulation.None
})
export class SidebarNavComponent extends AppComponentBase {

  menuItems: MenuItem[] = [
    new MenuItem(this.l('New Items'), 'Pages.Stats_New', 'playlist_add', '/stats/new')
];

  constructor(
    injector: Injector
) {
    super(injector);
}

showMenuItem(menuItem): boolean {
  if (menuItem.permissionName) {
      return this.permission.isGranted(menuItem.permissionName);
  }

  return true;
}

}
