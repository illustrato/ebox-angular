import { Component, OnInit, ViewEncapsulation, Injector } from '@angular/core';
import { TenantServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
  selector: 'top-bar',
  templateUrl: './topbar.component.html',
  encapsulation: ViewEncapsulation.None
})
export class TopbarComponent extends AppComponentBase implements OnInit {
  public keyword: string;
  tenantId = abp.multiTenancy.getTenantIdCookie();
  tenancyName = 'Business';
  constructor(
    injector: Injector,
    private _tenantService: TenantServiceProxy) {
    super(injector);
    console.log(`tenant: ${this.tenantId}`);
    console.log(`user: ${abp.session.userId}`);
    console.log(`user-tenant: ${abp.session.tenantId}`);
  }

  ngOnInit() {
    this._tenantService.get(this.tenantId)
      .subscribe(result => {
        this.tenancyName = result.tenancyName;
      });
  }

}
