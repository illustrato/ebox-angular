import { DeptDetailOutput, DepartmentServiceProxy, OrgDept, EmployeeDto } from '@shared/service-proxies/service-proxies';
import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/app-component-base';
import { Router, ActivatedRoute, Params } from '@angular/router';

import * as _ from 'lodash';
import { CreateDeptComponent } from '../create-dept/create-dept.component';

@Component({
  templateUrl: './dept-detail.component.html',
  animations: [appModuleAnimation()],
  providers: [DepartmentServiceProxy]
})
export class DeptDetailComponent extends AppComponentBase implements OnInit {

  @ViewChild('createDeptModal', { static: false }) createDeptModal: CreateDeptComponent;

  department: DeptDetailOutput = new DeptDetailOutput();
  employees: EmployeeDto[] = [];
  departmentName: string;
  departmentIcon: string;
  isDelete: boolean;
  deptId: string;

  constructor(
    private injector: Injector,
    private _deptService: DepartmentServiceProxy,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) { super(injector); }

  ngOnInit() {
    this._activatedRoute.params.subscribe((params: Params) => {
      this.deptId = params['deptId'];
      this.loadDepartment();
    });
  }

  updateDepartment(): void {
    this.createDeptModal.active = true;
    this.createDeptModal.modal.show();
  }


  removeDepartment(): void {
    const input = new OrgDept();
    input.id = this.department.id;

    this._deptService.delete(input.id)
      .subscribe(() => {
        abp.notify.info('Department Removed.');
        this.backToDepartmentsPage();
      });
  }

  isRegistered(): boolean {
    return _.some(this.department.employees, { userId: abp.session.userId });
  }

  loadDepartment() {
    this._deptService.getDetail(this.deptId)
      .subscribe((result: DeptDetailOutput) => {
        this.department = result;
        this.departmentName = result.department.deptName;
        this.departmentIcon = result.department.fontIcon;
        this.isDelete = result.department.isDeleted;
        this.employees = result.employees;
      });
  }

  backToDepartmentsPage() {
    this._router.navigate(['org/departments']);
  }

  refresh() {
    this.backToDepartmentsPage();
  }


}
