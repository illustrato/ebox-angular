import { Moment } from 'moment';
import {
  DepartmentServiceProxy,
  OrgDept,
  DeptListDto,
  ListResultDtoOfDeptListDto,
  OrgDeptListDto,
  ListResultDtoOfOrgDeptListDto
} from './../../shared/service-proxies/service-proxies';
import {
  Component,
  Injector,
  ViewChild
} from '@angular/core';
import {
  appModuleAnimation
} from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from '@shared/paged-listing-component-base';
import { CreateDeptComponent } from './create-dept/create-dept.component';


@Component({
  templateUrl: './departments.component.html',
  animations: [appModuleAnimation()],
  providers: [DepartmentServiceProxy]
})
export class DepartmentsComponent extends PagedListingComponentBase <DeptListDto> {

  @ViewChild('createDeptModal', {static: false}) createDeptModal: CreateDeptComponent;

  active = false;
  departments: OrgDeptListDto[] = [];
  includeDeletedDepartments = false;

  constructor(
    private injector: Injector,
    private _deptService: DepartmentServiceProxy
  ) {
    super(injector);
  }

  includeDeletedDepartmentsCheckboxChanged() {
    this.loadData();
  }

  // Show Modals
  createDepartment(): void {
    this.createDeptModal.show();
  }

  loadData() {
    this._deptService.getAssociation(this.includeDeletedDepartments)
          .subscribe((result: ListResultDtoOfOrgDeptListDto) => {
            this.departments = result.items;
          });
    }

  protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
    this.loadData();
    finishedCallback();
  }
  protected delete(entity: any): void {
    abp.message.confirm(
      'Are you sure you want to cancel this department?',
      (result: boolean) => {
          if (result) {
              this._deptService.delete(entity)
                  .subscribe(() => {
                      abp.notify.info('Department is deleted');
                      this.refresh();
                  });
          }
      }
  );
  }
}
