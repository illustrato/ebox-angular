import {
  AddDepartmentInput,
  DepartmentServiceProxy,
  DeptListDto
} from './../../../shared/service-proxies/service-proxies';
import {
  Component,
  Injector,
  ViewChild,
  ElementRef,
  EventEmitter,
  Output,
  OnInit
} from '@angular/core';
import {
  AppComponentBase
} from '@shared/app-component-base';
import {
  ModalDirective
} from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'create-dept-modal',
  templateUrl: './create-dept.component.html',
  providers: [DepartmentServiceProxy]
})
export class CreateDeptComponent extends AppComponentBase implements OnInit{

  @ViewChild('createDeptModal', {
    static: false
  }) modal: ModalDirective;
  @ViewChild('modalContent', {
    static: false
  }) modalContent: ElementRef;

  @Output() modalSave: EventEmitter < any > = new EventEmitter < any > ();

  active = false;
  saving = false;
  department: AddDepartmentInput;
  deptList: DeptListDto[] = [];

  deptId: string;

  constructor(
    private injector: Injector,
    private _deptService: DepartmentServiceProxy,
    private _router: Router,
    private _activatedRoute: ActivatedRoute) {
    super(injector);
  }

  ngOnInit(): void {

    this._activatedRoute.params.subscribe((params: Params) => {
      this.deptId = params['deptId'];

      this._deptService.getList(this.deptId ? this.deptId.substr(0, 2) : null)
    .subscribe((result) => {
      this.deptList = result.items;
    });
      if (this.deptId) {
        this._deptService.get(this.deptId)
        .subscribe((result) => {
          this.department = new AddDepartmentInput();
          this.department.init({isActive: true});
          this.department.id = this.deptId;
          this.department.tenantId = result.tenantId;
          this.department.description = result.description;
        });
      }
      // this.loadDepartment();
  });
  }

  show(): void {
    this.active = true;
    this.modal.show();
    this.department = new AddDepartmentInput();
    this.department.init({
      isActive: true
    });
  }
  onShown(): void {
    // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
  }
  save(): void {
    this.saving = true;

    if (!this.deptId) {
      this._deptService.create(this.department)
      .pipe(finalize(() => {
        this.saving = false;
      }))
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close();
        this.modalSave.emit(null);
      });
      return;
    }
    this._deptService.update(this.department)
    .pipe(finalize(() => {
      this.saving = false;
    }))
    .subscribe((reply) => {
      this.notify.info(this.l('SavedSuccessfully'));
      this.close();
      this.modalSave.emit(null);
    });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }

}
