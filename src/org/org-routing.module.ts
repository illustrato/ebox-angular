import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrgComponent } from './org.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { StaffComponent } from './staff/staff.component';
import { EmpDetailComponent } from './staff/emp-detail/emp-detail.component';
import { DepartmentsComponent } from './departments/departments.component';
import { RolesComponent } from './roles/roles.component';
import { DeptDetailComponent } from './departments/dept-detail/dept-detail.component';


const routes: Routes = [{
  path: '',
  component: OrgComponent,
  children: [
    { path: '', redirectTo: '/org/departments', pathMatch: 'full' },
    { path: 'departments', component: DepartmentsComponent, data: { permission: 'Pages.Departments' }, canActivate: [AppRouteGuard] },
    // tslint:disable-next-line: max-line-length
    { path: 'departments/:deptId', component: DeptDetailComponent, data: { permission: 'Pages.Departments' }, canActivate: [AppRouteGuard] },
    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Roles' }, canActivate: [AppRouteGuard] },
    { path: 'staff', component: StaffComponent, data: { permission: 'Pages.Staff' }, canActivate: [AppRouteGuard] },
    { path: 'staff/:empId', component: EmpDetailComponent, data: { permission: 'Pages.Staff' }, canActivate: [AppRouteGuard]  }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrgRoutingModule { }
