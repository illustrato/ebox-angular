import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrgRoutingModule } from './org-routing.module';

import { DepartmentsComponent } from './departments/departments.component';
import { EmpDMLComponent } from './staff/emp-dml/emp-dml.dialog.component';
import { StaffComponent } from './staff/staff.component';
import { EmpDetailComponent } from './staff/emp-detail/emp-detail.component';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { AbpModule } from 'abp-ng2-module/dist/src/abp.module';
import { SharedModule } from '@shared/shared.module';
import { ModalModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
// layout
import { SideBarNavComponent } from './layout/sidebar-nav.component';
import { TopBarComponent } from './layout/topbar.component';
import { OrgComponent } from './org.component';
import { RolesComponent } from './roles/roles.component';
import { CreateRoleDialogComponent } from './roles/create-role/create-role-dialog.component';
import { EditRoleDialogComponent } from './roles/edit-role/edit-role-dialog.component';
import { CreateDeptComponent } from './departments/create-dept/create-dept.component';
import { DeptDetailComponent } from './departments/dept-detail/dept-detail.component';


@NgModule({
  declarations: [
    OrgComponent,
    // layout
    TopBarComponent,

    SideBarNavComponent,
    DepartmentsComponent,
    StaffComponent,
    EmpDetailComponent,
    EmpDMLComponent,
    // roles
    RolesComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    CreateDeptComponent,
    DeptDetailComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    AbpModule,
    SharedModule,
    OrgRoutingModule,
    ServiceProxyModule,
    ModalModule.forRoot(),
    NgxPaginationModule
  ],
  entryComponents: [
    // roles
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // employee
    EmpDMLComponent
  ]
})
export class OrgModule { }
