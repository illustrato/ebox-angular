import { EmployeeDto, PagedResultDtoOfEmployeeDto } from './../../shared/service-proxies/service-proxies';
import { Component, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { EmployeeServiceProxy } from '@shared/service-proxies/service-proxies';
import { PagedRequestDto, PagedListingComponentBase } from '@shared/paged-listing-component-base';
import { MatDialog } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { EmpDMLComponent } from './emp-dml/emp-dml.dialog.component';

class PagedStaffRequestDto extends PagedRequestDto {
  keyword: string;
  departmentId: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './staff.component.html',
  animations: [appModuleAnimation()],
  providers: [EmployeeServiceProxy],
  styles: [
    `
      mat-form-field {
        padding: 10px;
      }
    `
  ]
})
export class StaffComponent extends PagedListingComponentBase<EmployeeDto>  {
  staff: EmployeeDto[] = [];
  keyword = '';
  departmentId = '';
  isActive: boolean | null;

  constructor(
    injector: Injector,
    private _empService: EmployeeServiceProxy,
    private _dialog: MatDialog
  ) {
    super(injector);
  }

  createEmp(): void {
    this.showCreateOrEditEmpDialog();
  }

  editEmp(emp: EmployeeDto): void {
    this.showCreateOrEditEmpDialog(emp.id);
  }

  public resetPassword(emp: EmployeeDto): void {
    // this.showResetPasswordUserDialog(user.id);
  }

  protected list(request: PagedStaffRequestDto, pageNumber: number, finishedCallback: Function): void {

    request.keyword = this.keyword;
    request.isActive = this.isActive;
    request.departmentId = this.departmentId;

    this._empService
      .getAll(request.keyword, request.isActive, request.departmentId, request.skipCount, request.maxResultCount)
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: PagedResultDtoOfEmployeeDto) => {
        this.staff = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(emp: EmployeeDto): void {
    abp.message.confirm(
      this.l(`you are about to delete ${emp.user.fullName}'s record`, emp.user.fullName),
      (result: boolean) => {
        if (result) {
          this._empService.delete(emp.user.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showCreateOrEditEmpDialog(id?: number): void {
    let createOrEditEmpDialog;
    if (id === undefined || id <= 0) {
      createOrEditEmpDialog = this._dialog.open(EmpDMLComponent);
    } else {
      createOrEditEmpDialog = this._dialog.open(EmpDMLComponent, {
        data: id
      });
    }

    createOrEditEmpDialog.afterClosed().subscribe(result => {
      if (result) {
        this.refresh();
      }
    });
  }

}
