import { Component, OnInit, Injector, Optional, Inject, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import {
  CreateEmployeeDto,
  EmployeeDto,
  OrgDeptListDto,
  EmployeeServiceProxy,
  UserServiceProxy,
  DepartmentServiceProxy,
  RoleDto,
  UserDto,
  ListResultDtoOfOrgDeptListDto
} from '@shared/service-proxies/service-proxies';
import { MAT_DIALOG_DATA, MatDialogRef, MatCheckboxChange, MatSelect } from '@angular/material';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './emp-dml.dialog.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
      mat-checkbox {
        padding-bottom: 5px;
      }
    `
  ],
  providers: [DepartmentServiceProxy, EmployeeServiceProxy]
})
export class EmpDMLComponent extends AppComponentBase implements OnInit {

@ViewChild('cmbDeptId', {static: false}) cmbDeptId: MatSelect;

  saving = false;
  employee: any;
  roles: RoleDto[] = [];
  departments: OrgDeptListDto[] = [];
  checkedRolesMap: { [key: string]: boolean } = {};
  defaultRoleCheckedStatus = false;

  constructor(
    injector: Injector,
    public _empService: EmployeeServiceProxy,
    public _userService: UserServiceProxy,
    private _dialogRef: MatDialogRef<EmpDMLComponent>,
    private _deptService: DepartmentServiceProxy,
    @Optional() @Inject(MAT_DIALOG_DATA) public _id: number) {
      super(injector);
      if (this._id) {
        this.employee  = new EmployeeDto();
      } else {
        this.employee = new CreateEmployeeDto();
      }
      this.employee.user = new UserDto();
    }

  ngOnInit(): void {
    this._deptService.getAssociation(false).subscribe((result: ListResultDtoOfOrgDeptListDto) => {
      this.departments = result.items;
    });

    if (!this._id) {
        this.employee.user.isActive = true;

        this._userService.getRoles().subscribe(role => {
          this.roles = role.items;
          this.setInitialRolesStatus();
        });
        return;
      }

      this._empService.get(this._id).subscribe(emp => {
        this.employee = emp;

        this._userService.getRoles().subscribe(role => {
        this.roles = role.items;
        this.setInitialRolesStatus();
        });
      });
  }

  setInitialRolesStatus(): void {
    _.map(this.roles, item => {
      this.checkedRolesMap[item.normalizedName] = this.isRoleChecked(
        item.normalizedName
      );
    });
  }

  isRoleChecked(normalizedName: string): boolean {
    // just return default role checked status
    // it's better to use a setting
    if (this._id) {
      return _.includes(this.employee.user.roleNames, normalizedName);
    }
    return this.defaultRoleCheckedStatus;
  }
  onRoleChange(role: RoleDto, $event: MatCheckboxChange) {
    this.checkedRolesMap[role.normalizedName] = $event.checked;
  }

  getCheckedRoles(): string[] {
    const roles: string[] = [];
    _.forEach(this.checkedRolesMap, function(value, key) {
      if (value) {
        roles.push(key);
      }
    });
    return roles;
  }

  save(): void {
    this.saving = true;
    this.employee.user.roleNames = this.getCheckedRoles();

    if (!this._id) {
      this._empService.create(this.employee)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });

    } else {
      this._empService.update(this.employee)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
    }
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
